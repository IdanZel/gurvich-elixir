defmodule ForPaz do
  use Application
  alias ForPaz.Compress
  alias ForPaz.Sort
  alias ForPaz.Stringify

  def start(_type, _args) do
    output_path = "D:\\Code\\for_paz\\output\\output.txt"
    feed_path = "https://www.kan.org.il/rss/allnews.ashx"
    write_feed(output_path, feed_path)

    Task.start(fn -> nil end)
  end

  def write_feed(output_path, feed_path) do
    feed =
      feed_path
      |> Scrape.feed
      |> Compress.compress_feed
      |> Sort.sort_feed
      |> Stringify.stringify_feed

    File.write(output_path, feed)
  end
end
