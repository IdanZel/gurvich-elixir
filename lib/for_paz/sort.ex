defmodule ForPaz.Sort do
  def sort_feed(feed) do
    Enum.sort(feed, fn x, y ->
      case DateTime.compare(x[:date], y[:date]) do
        :gt -> true
        _ -> false
      end
    end)
  end
end
