defmodule ForPaz.Compress do
  def compress_feed(output, []), do: output

  def compress_feed(output, [head | tail]) do
    feed_item = %{
      title: head[:title],
      desc: head[:description],
      date: head[:pubdate]
    }

    compress_feed(output ++ [feed_item], tail)
  end

  def compress_feed(feed) do
    compress_feed([], feed)
  end
end
