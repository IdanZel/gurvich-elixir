defmodule ForPaz.Stringify do
  def stringify_feed(output, []), do: output

  def stringify_feed(output, [head | tail]) do
    stringify_feed("#{output}\n\n#{head[:title]} - #{head[:desc]} (#{head[:date]})", tail)
  end

  def stringify_feed(feed) do
    stringify_feed([], feed)
  end
end
